(function(){
'use strict';


angular.module('demo')
  .controller('HomeCtrl', ['$scope','Products','$state',function ($scope, Products,$state) {
  
  console.log('test');
  $scope.selected = undefined;
  $scope.showAlert = false;
  $scope.products = Products;
  
  $scope.goToSearchResult = function () {
    console.log(typeof $scope.selected);
    if(typeof $scope.selected == 'object'){
      $scope.showAlert = false;
      $state.go('searchList', $scope.selected);
    }
    else{
      $scope.showAlert = true;
    }
  };

  }]);
}());

