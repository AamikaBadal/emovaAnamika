(function(){

	'use strict';


angular.module('demo')
	  .controller('SearchresultCtrl',['$rootScope','$scope','$stateParams','$state',function ($rootScope,$scope, $stateParams,$state) {
	  	console.log($stateParams);
	    $scope.selectedProduct = $stateParams;
	    $rootScope.logedIn=false;
	    $scope.goToCheckoutPage = function () {
	    if($rootScope.logedIn){
	    	$state.go('checkout');
	    }else{
	    	$state.go('login');
	    }
	};
  }]);
}());

