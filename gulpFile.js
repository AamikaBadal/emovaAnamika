var gulp=require('gulp');
var gulpif=require('gulp-if');
var concat=require('gulp-concat');
var sourcemaps=require('gulp-sourcemaps');
var uglify=require('gulp-uglify');
var util=require('gulp-util');
var minify=require('gulp-minify');
var pump=require('pump');
var babel=require('gulp-babel');
var sass=require('gulp-sass');
var wiredep = require('gulp-wiredep');
var angularFilesort = require('gulp-angular-filesort');
var inject = require('gulp-inject');
var browserify=require('browserify');
var source=require('vinyl-source-stream');
var useref=require('gulp-useref');
var buffer=require('vinyl-buffer');
var rename=require('gulp-rename');
var clean=require('gulp-clean');
var package = require('./package.json');

gulp.task('insert', function () {

  var injectStyles = gulp.src([
       './app/styles/*.css'
    ]
  );
   var injectScripts = gulp.src(['./app/app.js',
       './app/scripts/**/*.js'
     ]);
  
 

  return gulp.src("./app/test.html")
    .pipe(inject(injectStyles))
    .pipe(inject(injectScripts))
    .pipe(gulp.dest('./app')); 
 
});

gulp.task('clean',function(){
	return gulp.src(['./app/dist/*.js','./app/test.html'],
		             {
		             	read:false
		             }).pipe(clean());
});


gulp.task('compress',function(){
	
	return gulp.src([
          './app/app.js',
          './app/scripts/**/*.js'          
		])
	    .pipe(sourcemaps.init())
	    .pipe(concat('app.min.js'))
	    .pipe(uglify({mangle:true}).on('error',function(e){
	    	console.log(e)
	    }))
	    .pipe(sourcemaps.write())
	    .pipe(gulp.dest('./app/dist/'))
});

gulp.task('sass', function () {
  return gulp.src('./app/styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/styles/'));
});

gulp.task('bundle-vendor',function(){
	return browserify({
			entries:['./app/external/vendor.js']
			})
	        .bundle()
	       .on('error',function(e){
	       	console.log(e);
	       })
	       .pipe(source('vendor.min.js'))
	       .pipe(buffer())
	       .pipe(uglify({mangle:false}))
	       .pipe(gulp.dest('./app/external/'));
});

gulp.task('inject-min',function(){
	return gulp.src('./app/index.html')
    .pipe(inject(gulp.src('./app/dist/app.min.js')))
    .pipe(gulp.dest('./app/')); 
})


gulp.task('build-dev',['insert','sass','bundle-vendor']);
gulp.task('build-prod',['inject-min','compress','clean']);